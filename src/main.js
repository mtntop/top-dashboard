import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faUserSecret);

Vue.component("font-awesome-icon", FontAwesomeIcon);

const router = new VueRouter({
  routes: [],
  mode: "history"
});

import "bootstrap/dist/css/bootstrap.css";
import '../assets/fontawesome/css/all.css';

Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router: router
}).$mount("#app");
